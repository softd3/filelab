/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aritsu
 */
public class WritePlayer {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Player o = new Player('O');
            Player x = new Player('X');
            o.win();
            x.win();
            o.lose();
            x.lose();
            o.draw();
            x.draw();
            System.out.println(o);
            System.out.println(x);
            
            
            File file = new File("player.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
            
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
