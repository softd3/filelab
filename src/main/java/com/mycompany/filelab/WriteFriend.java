/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aritsu
 */
public class WriteFriend {
    public static void main(String[] args) {
        ArrayList<Friend> friendList = new ArrayList<>();
        
        
        friendList.add(new Friend("Eiei", 99, "0631234567"));
        friendList.add(new Friend("Euei", 99, "0637654321"));
        friendList.add(new Friend("Eieu", 99, "0737654321"));
        
        FileOutputStream fos = null;
        
        
        
        try {
            
            
            File file = new File("friends.dat");
            
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friendList);
            
            
            
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
}
